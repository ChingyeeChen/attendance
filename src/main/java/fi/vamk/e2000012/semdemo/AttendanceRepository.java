package fi.vamk.e2000012.semdemo;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance,Integer>{
	public Attendance findByKey(String key);

}